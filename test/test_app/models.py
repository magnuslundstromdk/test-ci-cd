from django.db import models

# Create your models here.


class Product(models.Model):
    price = models.IntegerField()
    stock_count = models.IntegerField()

    def __str__(self):
        return f"{self.price} - {self.stock_count}"

    def is_in_stock(self):
        if self.stock_count <= 0:
            return False
        return True