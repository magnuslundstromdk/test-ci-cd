from django.test import TestCase, SimpleTestCase
from django.urls import reverse, resolve
from .models import Product
from .views import index

# Create your tests here.


class TestUrls(SimpleTestCase):
    def test_app_url_index(self):
        url = reverse("test_app:index")
        self.assertEquals(resolve(url).func, index)


class ProductModelTests(TestCase):
    def test_is_not_in_stock(self):
        product = Product(price=200, stock_count=0)

        self.assertIs(product.is_in_stock(), False)

    def test_is_in_stock(self):
        product = Product(price=100, stock_count=1)

        self.assertIs(product.is_in_stock(), True)


def create_product(price: int, stock_count: int):
    Product.objects.create(price=price, stock_count=stock_count)


class ProductIndexViewTest(TestCase):
    def test_no_products(self):
        """
        If no products exists, and approiate message is displayed.
        """
        response = self.client.get(reverse("test_app:index"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No products")
        self.assertQuerysetEqual(response.context["products"], [])

    def test_product_no_stock(self):
        create_product(200, 0)
        response = self.client.get(reverse("test_app:index"))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context["products"], ["<Product: 200 - 0>"])
        self.assertContains(response, "200")
        self.assertContains(response, "Not in stock")

    def test_products_not_in_stock_and_in_stock(self):
        create_product(200, 0)
        create_product(100, 10)
        response = self.client.get(reverse("test_app:index"))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(
            response.context["products"],
            ["<Product: 200 - 0>", "<Product: 100 - 10>"],
            ordered=False,
        )
        self.assertContains(response, "In stock")
        self.assertContains(response, "Not in stock")
